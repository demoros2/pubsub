from setuptools import setup

package_name = 'pubsub'
package_maintainer = 'archerearthx'
package_maintainer_email = 'archerearthx@gmail.com'
package_description = 'Demo for topics'
package_license = 'Apache License 2.0'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer=package_maintainer,
    maintainer_email=package_maintainer_email,
    description=package_description,
    license=package_license,
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'talker = pubsub.publisher_member_function:main',
            'listener = pubsub.subscriber_member_function:main',
        ],
    },
)
